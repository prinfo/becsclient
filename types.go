package becsclient

//Object BECS type
type Object struct {
	OID         uint64 `xml:"oid,omitempty"`
	Parentoid   uint64 `xml:"parentoid,omitempty"`
	Creator     string `xml:"creator,omitempty"`
	Ctime       uint   `xml:"ctime,omitempty"`
	Modifier    string `xml:"modifier,omitempty"`
	Mtime       uint   `xml:"mtime,omitempty"`
	Class       string `xml:"class,omitempty"`
	Name        string `xml:"name,omitempty"`
	Namespace   string `xml:"namespace,omitempty"`
	Servicename string `xml:"servicename,omitempty"`
	Weight      uint   `xml:"weight,omitempty"`
	Flags       string `xml:"flags,omitempty"`
	Role        string `xml:"role,omitempty"`
	Contextname string `xml:"contextname,omitempty"`
	Config      string `xml:"config,omitempty"`
	Elementtype string `xml:"elementtype,omitempty"`
	Elementrole string `xml:"elementrole,omitempty"`
	Conditional string `xml:"conditional,omitempty"`
	Parameters  *struct {
		Items []ParameterKey `xml:"item"`
	} `xml:"parameters,omitempty"`

	Linkid   uint64    `xml:"linkid,omitempty"`
	Resource *Resource `xml:"resource,omitempty"`

	Opaque *struct {
		Items []ParameterKey `xml:"item"`
	} `xml:"opaque,omitempty"`

	Status  string `xml:"status,omitempty"`
	Nchilds uint   `xml:"nchilds,omitempty"`
}

//Resource BECS type
type Resource struct {
	Rcparentoid    uint64 `xml:"rcparentoid,omitempty"`
	Address        string `xml:"address,omitempty"`
	Prefixlen      uint   `xml:"prefixlen,omitempty"`
	Reservebegin   uint   `xml:"reservebegin,omitempty"`
	Parameterindex string `xml:"parameterindex,omitempty"`
	Start          uint64 `xml:"start,omitempty"`
	Size           uint64 `xml:"size,omitempty"`
	Parametername  string `xml:"parametername,omitempty"`
}

//ParameterKey BECS type
type ParameterKey struct {
	Name   string `xml:"name"`
	Values struct {
		Items []ParameterValue `xml:"item"`
	} `xml:"values,omitempty"`
}

//ParameterValue BECS type
type ParameterValue struct {
	Value string `xml:"value"`
	Index string `xml:"index,omitempty"`
}

//ElementStatus BECS type
type ElementStatus struct {
	OID              uint64 `xml:"oid"`
	Status           string `xml:"status"`
	Laststatuschange struct {
		Seconds int64 `xml:"seconds"`
	} `xml:"laststatuschange,omitempty"`

	Version string `xml:"version,omitempty"`
	Image   string `xml:"image,omitempty"`
	Uptime  struct {
		Seconds int64 `xml:"seconds"`
	} `xml:"uptime,omitempty"`
}

//Client BECS type
type Client struct {
	Context struct {
		OID       uint64 `xml:"oid"`
		StartOID  uint64 `xml:"startoid"`
		Name      string `xml:"name"`
		Selectors struct {
			Items []NameValue `xml:"item"`
		} `xml:"selectors"`
	} `xml:"context"`
	Node          uint64 `xml:"node"`
	Serviceattach uint64 `xml:"serviceattach,omitempty"`
	IP            string `xml:"ip,omitempty"`
	Status        string `xml:"status"`
}

//TypeNameValue BECS type
type TypeNameValue struct {
	Type  string `xml:"type,omitempty"`
	Name  string `xml:"name,omitempty"`
	Value string `xml:"value"`
}

//NameValue BECS type
type NameValue struct {
	Name  string `xml:"name"`
	Value string `xml:"value"`
}

//StatusInfo BECS type
type StatusInfo struct {
	Name        string `xml:"name"`
	Type        string `xml:"type"`
	Category    string `xml:"category"`
	Index       string `xml:"index"`
	Value       string `xml:"value"`
	Description string `xml:"description"`
}

//ServiceMapAttribute BECS type
type ServiceMapAttribute struct {
	Name            string   `xml:"name"`
	Type            string   `xml:"type,omitempty"`
	Index           string   `xml:"index,omitempty"`
	Constraint      string   `xml:"constraint,omitempty"`
	Description     string   `xml:"description,omitempty"`
	CurrentValue    string   `xml:"currentValue,omitempty"`
	AvailableValues []string `xml:"availableValues,omitempty"`
}

type Metric struct {
	Name   string `xml:"name"`
	Type   string `xml:"type"`
	Values struct {
		Items []MetricValue `xml:"item"`
	} `xml:"values,omitempty"`
}

type MetricValue struct {
	Labels struct {
		Items []MetricLabel `xml:"item"`
	} `xml:"labels,omitempty"`
	Value uint64 `xml:"value"`
}

type MetricLabel struct {
	Name  string `xml:"name,omitempty"`
	Value string `xml:"value,omitempty"`
}

type Application struct {
	Displayname  string
	Hostname     string
	Version      string
	UpTime       uint
	StartTime    uint
	CPUUsage     uint
	CPUAverage60 uint
	MemoryPools  []MemoryPool
}

type MemoryPool struct {
	Name       string `xml:"name"`
	Size       uint   `xml:"size"`
	Out        uint   `xml:"out"`
	Pages      uint   `xml:"pages"`
	EmptyPages uint   `xml:"emptypages"`
}
