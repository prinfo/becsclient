package becsclient

import (
	"bytes"
	"encoding/xml"
	"errors"
	"net/http"
	"time"
)

var (
	httpClient  http.Client
	becsURL     string
	soapRequest = envelope{
		Env:  "http://schemas.xmlsoap.org/soap/envelope/",
		Becs: "urn:packetfront_becs",
	}
)

func init() {
	httpClient.Timeout = time.Second * 60
	httpClient.Transport = &http.Transport{
		MaxConnsPerHost: 250,
		IdleConnTimeout: time.Minute * 10,
	}
}

type envelope struct {
	XMLName xml.Name `xml:"soapenv:Envelope"`
	Env     string   `xml:"xmlns:soapenv,attr"`
	Becs    string   `xml:"xmlns:becs,attr"`
	Header  struct {
		Request struct {
			SessionID string `xml:"sessionid"`
		} `xml:"urn:requestHeader"`
	} `xml:"soapenv:Header"`
	Body struct {
		Method interface{} `xml:"method"`
	} `xml:"body"`
}

func soapCall(request []byte, response interface{}) error {
	if len(becsURL) == 0 {
		return errors.New("No BECS URL")
	}

	resp, err := httpClient.Post(becsURL, "text/xml; charset=utf-8", bytes.NewBuffer(request))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	err = xml.NewDecoder(resp.Body).Decode(response)
	if err != nil {
		return errors.New("Decode: " + err.Error())
	}

	return nil
}

//SessionLogin logs in to BECS and creates a session that lasts for 10 mins
func SessionLogin(url, username, password string) error {
	becsURL = url

	var sessionLogin struct {
		XMLName xml.Name `xml:"becs:sessionLogin"`
		In      struct {
			Type     string `xml:"xsi:type,attr"`
			Username string `xml:"username"`
			Password string `xml:"password"`
		} `xml:"in"`
	}
	sessionLogin.In.Type = "becs:sessionLoginIn"
	sessionLogin.In.Username = username
	sessionLogin.In.Password = password

	soapRequest.Body.Method = sessionLogin
	req, err := xml.Marshal(soapRequest)
	if err != nil {
		return err
	}

	var sessionLoginResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err       uint   `xml:"err"`
					ErrTxt    string `xml:"errtxt,omitempty"`
					SessionID string `xml:"sessionid"`
				} `xml:"out"`
			} `xml:"sessionLoginResponse"`
		}
	}
	err = soapCall(req, &sessionLoginResponse)
	if err != nil {
		return err
	}

	if sessionLoginResponse.Body.Response.Out.Err != 0 {
		return errors.New(sessionLoginResponse.Body.Response.Out.ErrTxt)
	}

	soapRequest.Header.Request.SessionID = sessionLoginResponse.Body.Response.Out.SessionID

	return nil
}

//SessionPing pings BECS to keep the session alive for another 10 minutes
func SessionPing() error {
	var sessionPing struct {
		XMLName xml.Name `xml:"becs:sessionPing"`
		In      struct {
			Type string `xml:"xsi:type,attr"`
		} `xml:"in"`
	}
	sessionPing.In.Type = "becs:sessionPingIn"

	req := soapRequest
	req.Body.Method = sessionPing
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var sessionPingResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"sessionPingResponse"`
		}
	}
	err = soapCall(data, &sessionPingResponse)
	if err != nil {
		return err
	}

	if sessionPingResponse.Body.Response.Out.Err != 0 {
		return errors.New(sessionPingResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//ObjectFind finds objects based on search attributes
func ObjectFind(search Object) ([]Object, error) {
	var objectFind struct {
		XMLName xml.Name `xml:"becs:objectFind"`
		In      struct {
			Type    string `xml:"xsi:type,attr"`
			Queries struct {
				Queries []Object `xml:"queries"`
			} `xml:"queries"`
		} `xml:"in"`
	}
	objectFind.In.Type = "becs:objectFindIn"
	objectFind.In.Queries.Queries = append(objectFind.In.Queries.Queries, search)

	req := soapRequest
	req.Body.Method = objectFind
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, err
	}

	var objectFindResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err     uint   `xml:"err"`
					ErrTxt  string `xml:"errtxt,omitempty"`
					Objects struct {
						Items []Object `xml:"item"`
					} `xml:"objects"`
				} `xml:"out"`
			} `xml:"objectFindResponse"`
		}
	}
	err = soapCall(data, &objectFindResponse)
	if err != nil {
		return nil, err
	}

	if objectFindResponse.Body.Response.Out.Err != 0 {
		return nil, errors.New(objectFindResponse.Body.Response.Out.ErrTxt)
	}

	return objectFindResponse.Body.Response.Out.Objects.Items, nil
}

//ObjectCreate creates an object
func ObjectCreate(jobid uint64, object Object) (uint64, error) {
	var objectCreate struct {
		XMLName xml.Name `xml:"becs:objectCreate"`
		In      struct {
			JobID  uint64 `xml:"jobid"`
			Object Object `xml:"object"`
		} `xml:"in"`
	}
	objectCreate.In.JobID = jobid
	objectCreate.In.Object = object

	req := soapRequest
	req.Body.Method = objectCreate
	data, err := xml.Marshal(req)
	if err != nil {
		return 0, err
	}

	var objectCreateResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					OID    uint64 `xml:"oid"`
				} `xml:"out"`
			} `xml:"objectCreateResponse"`
		}
	}
	err = soapCall(data, &objectCreateResponse)
	if err != nil {
		return 0, err
	}

	if objectCreateResponse.Body.Response.Out.Err != 0 {
		return 0, errors.New(objectCreateResponse.Body.Response.Out.ErrTxt)
	}

	return objectCreateResponse.Body.Response.Out.OID, nil
}

//ObjectModify modifies an object
func ObjectModify(jobid uint64, object Object) error {
	var objectModify struct {
		XMLName xml.Name `xml:"becs:objectModify"`
		In      struct {
			JobID  uint64 `xml:"jobid"`
			Object Object `xml:"object"`
		} `xml:"in"`
	}
	objectModify.In.JobID = jobid
	objectModify.In.Object = object

	req := soapRequest
	req.Body.Method = objectModify
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var objectModifyResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"objectModifyResponse"`
		}
	}
	err = soapCall(data, &objectModifyResponse)
	if err != nil {
		return err
	}

	if objectModifyResponse.Body.Response.Out.Err != 0 {
		return errors.New(objectModifyResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//ServiceTreeFind searches and collects service information
func ServiceTreeFind(oid uint64, servicename, namespace string) ([]Object, error) {
	var serviceTreeFind struct {
		XMLName xml.Name `xml:"becs:serviceTreeFind"`
		In      struct {
			Type        string `xml:"xsi:type,attr"`
			OID         uint64 `xml:"oid"`
			ServiceName string `xml:"servicename,omitempty"`
			Namespace   string `xml:"namespace,omitempty"`
		} `xml:"in"`
	}
	serviceTreeFind.In.Type = "becs:serviceTreeFindIn"
	serviceTreeFind.In.OID = oid
	serviceTreeFind.In.ServiceName = servicename
	serviceTreeFind.In.Namespace = namespace

	req := soapRequest
	req.Body.Method = serviceTreeFind
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, err
	}

	var serviceTreeFindResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err     uint   `xml:"err"`
					ErrTxt  string `xml:"errtxt,omitempty"`
					Objects struct {
						Items []Object `xml:"item"`
					} `xml:"objects"`
				} `xml:"out"`
			} `xml:"serviceTreeFindResponse"`
		}
	}
	err = soapCall(data, &serviceTreeFindResponse)
	if err != nil {
		return nil, err
	}

	if serviceTreeFindResponse.Body.Response.Out.Err != 0 {
		return nil, errors.New(serviceTreeFindResponse.Body.Response.Out.ErrTxt)
	}

	return serviceTreeFindResponse.Body.Response.Out.Objects.Items, nil
}

//ObjectTreeFind finds objects based on object tree. If walkdown is > 0 it will traverse down the tree that many levels
func ObjectTreeFind(oid uint64, classMask string, walkdown uint) ([]Object, error) {
	var objectTreeFind struct {
		XMLName xml.Name `xml:"becs:objectTreeFind"`
		In      struct {
			Type      string `xml:"xsi:type,attr"`
			OID       uint64 `xml:"oid"`
			ClassMask string `xml:"classmask,omitempty"`
			WalkDown  uint   `xml:"walkdown"`
		} `xml:"in"`
	}
	objectTreeFind.In.Type = "becs:objectTreeFindIn"
	objectTreeFind.In.OID = oid
	objectTreeFind.In.ClassMask = classMask
	objectTreeFind.In.WalkDown = walkdown

	req := soapRequest
	req.Body.Method = objectTreeFind
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, err
	}

	var objectTreeFindResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err     uint   `xml:"err"`
					ErrTxt  string `xml:"errtxt,omitempty"`
					Objects struct {
						Items []Object `xml:"item"`
					} `xml:"objects"`
				} `xml:"out"`
			} `xml:"objectTreeFindResponse"`
		}
	}
	err = soapCall(data, &objectTreeFindResponse)
	if err != nil {
		return nil, err
	}

	if objectTreeFindResponse.Body.Response.Out.Err != 0 {
		return nil, errors.New(objectTreeFindResponse.Body.Response.Out.ErrTxt)
	}

	return objectTreeFindResponse.Body.Response.Out.Objects.Items, nil
}

//ObjectInterfaceFind collects interface oids
func ObjectInterfaceFind(oid uint64) ([]uint64, error) {
	var objectInterfaceFind struct {
		XMLName xml.Name `xml:"becs:objectInterfaceFind"`
		In      struct {
			Type string `xml:"xsi:type,attr"`
			OID  uint64 `xml:"oid"`
		} `xml:"in"`
	}
	objectInterfaceFind.In.Type = "becs:objectInterfaceFindIn"
	objectInterfaceFind.In.OID = oid

	req := soapRequest
	req.Body.Method = objectInterfaceFind
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, err
	}

	var objectInterfaceFindResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					OIDS   struct {
						Items []uint64 `xml:"item,omitempty"`
					} `xml:"oids"`
				} `xml:"out"`
			} `xml:"objectInterfaceFindResponse"`
		}
	}
	err = soapCall(data, &objectInterfaceFindResponse)
	if err != nil {
		return nil, err
	}

	if objectInterfaceFindResponse.Body.Response.Out.Err != 0 {
		return nil, errors.New(objectInterfaceFindResponse.Body.Response.Out.ErrTxt)
	}

	return objectInterfaceFindResponse.Body.Response.Out.OIDS.Items, nil

}

/*ClientFind searches for clients based on an IPv4/IPv6 prefix given by the ip field, or from an object in the BECS
tree given by the oid field. If ip is given and oid is > 0, only the oid field is used.

If limit is > 0 it will return an array with that many client, the actual number will be return as an uint*/
func ClientFind(ip string, oid uint64, limit uint) ([]Client, uint, error) {
	var clientFind struct {
		XMLName xml.Name `xml:"becs:clientFind"`
		In      struct {
			Type  string `xml:"xsi:type,attr"`
			IP    string `xml:"ip,omitempty"`
			OID   uint64 `xml:"oid,omitempty"`
			Limit uint   `xml:"limit,omitempty"`
		} `xml:"in"`
	}
	clientFind.In.Type = "becs:clientFindIn"
	clientFind.In.IP = ip
	clientFind.In.OID = oid
	clientFind.In.Limit = limit

	req := soapRequest
	req.Body.Method = clientFind
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, 0, err
	}

	var clientFindResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err     uint   `xml:"err"`
					ErrTxt  string `xml:"errtxt,omitempty"`
					Clients struct {
						Items []Client `xml:"item"`
					} `xml:"clients,omitempty"`
					Actual uint `xml:"actual"`
				} `xml:"out"`
			} `xml:"clientFindResponse"`
		}
	}
	err = soapCall(data, &clientFindResponse)
	if err != nil {
		return nil, 0, err
	}

	if clientFindResponse.Body.Response.Out.Err != 0 {
		return nil, 0, errors.New(clientFindResponse.Body.Response.Out.ErrTxt)
	}

	return clientFindResponse.Body.Response.Out.Clients.Items, clientFindResponse.Body.Response.Out.Actual, nil
}

//ElementStatusGet returns the current status for elements
func ElementStatusGet(cell string, oids []uint64) ([]ElementStatus, error) {
	var elementStatusGet struct {
		XMLName xml.Name `xml:"becs:elementStatusGet"`
		In      struct {
			Type string `xml:"xsi:type,attr"`
			Cell string `xml:"cell"`
			OIDS struct {
				OIDS []uint64 `xml:"oids"`
			} `xml:"oids"`
		} `xml:"in"`
	}
	elementStatusGet.In.Type = "becs:elementStatusGetIn"
	elementStatusGet.In.Cell = cell
	elementStatusGet.In.OIDS.OIDS = oids

	req := soapRequest
	req.Body.Method = elementStatusGet
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, err
	}

	var elementStatusGetResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					Status struct {
						Items []ElementStatus `xml:"item"`
					} `xml:"status"`
				} `xml:"out"`
			} `xml:"elementStatusGetResponse"`
		}
	}
	err = soapCall(data, &elementStatusGetResponse)
	if err != nil {
		return nil, err
	}

	if elementStatusGetResponse.Body.Response.Out.Err != 0 {
		return nil, errors.New(elementStatusGetResponse.Body.Response.Out.ErrTxt)
	}

	return elementStatusGetResponse.Body.Response.Out.Status.Items, nil
}

//InterfaceInfoGet returns the current status for an ASR interface
func InterfaceInfoGet(ifid TypeNameValue, depth uint, categories []string,
	namespace string, variables []NameValue) ([]StatusInfo, []NameValue, error) {

	var interfaceInfoGet struct {
		XMLName xml.Name `xml:"becs:interfaceInfoGet"`
		In      struct {
			Type       string        `xml:"xsi:type,attr"`
			IFID       TypeNameValue `xml:"ifid"`
			Depth      uint          `xml:"depth,omitempty"`
			Categories struct {
				Categories []string `xml:"categories"`
			} `xml:"categories,omitempty"`

			Namespace string `xml:"namespace,omitempty"`

			Variables struct {
				Variables []NameValue `xml:"variables"`
			} `xml:"variables,omitempty"`
		} `xml:"in"`
	}
	interfaceInfoGet.In.Type = "becs:interfaceInfoGetIn"
	interfaceInfoGet.In.IFID = ifid
	interfaceInfoGet.In.Depth = depth
	interfaceInfoGet.In.Categories.Categories = categories
	interfaceInfoGet.In.Namespace = namespace
	interfaceInfoGet.In.Variables.Variables = variables

	req := soapRequest
	req.Body.Method = interfaceInfoGet
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, nil, err
	}

	var interfaceInfoGetResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					Info   struct {
						Items []StatusInfo `xml:"item"`
					} `xml:"info,omitempty"`

					Categories struct {
						Items []NameValue `xml:"item"`
					} `xml:"categories,omitempty"`
				} `xml:"out"`
			} `xml:"interfaceInfoGetResponse"`
		}
	}
	err = soapCall(data, &interfaceInfoGetResponse)
	if err != nil {
		return nil, nil, err
	}

	if interfaceInfoGetResponse.Body.Response.Out.Err != 0 {
		return nil, nil, errors.New(interfaceInfoGetResponse.Body.Response.Out.ErrTxt)
	}

	return interfaceInfoGetResponse.Body.Response.Out.Info.Items, interfaceInfoGetResponse.Body.Response.Out.Categories.Items, nil
}

//ContextReset resets a single context
func ContextReset(oid uint64, name, reason string) error {
	var contextReset struct {
		XMLName xml.Name `xml:"becs:contextReset"`
		In      struct {
			Type    string `xml:"xsi:type,attr"`
			Context struct {
				Name string `xml:"name"`
				OID  uint64 `xml:"oid"`
			} `xml:"context"`
			Reason string `xml:"reason,omitempty"`
		} `xml:"in"`
	}
	contextReset.In.Type = "becs:contextResetIn"
	contextReset.In.Context.Name = name
	contextReset.In.Context.OID = oid
	contextReset.In.Reason = reason

	req := soapRequest
	req.Body.Method = contextReset
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var contextResetResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"contextResetResponse"`
		}
	}
	err = soapCall(data, &contextResetResponse)
	if err != nil {
		return err
	}

	if contextResetResponse.Body.Response.Out.Err != 0 {
		return errors.New(contextResetResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//ServiceMapCreate maps a service to an interface
func ServiceMapCreate(paroid uint64, service, namespace string, attributes ...ServiceMapAttribute) (uint64, error) {
	var serviceMapCreate struct {
		XMLName xml.Name `xml:"becs:serviceMapCreate"`
		In      struct {
			Type        string `xml:"xsi:type,attr"`
			Paroid      uint64 `xml:"paroid"`
			ServiceName string `xml:"svcname"`
			Namespace   string `xml:"namespace"`
			Attributes  struct {
				Attributes []ServiceMapAttribute `xml:"attributes"`
			} `xml:"attributes"`
		} `xml:"in"`
	}
	serviceMapCreate.In.Type = "becs:serviceMapCreateIn"
	serviceMapCreate.In.Paroid = paroid
	serviceMapCreate.In.ServiceName = service
	serviceMapCreate.In.Namespace = namespace
	serviceMapCreate.In.Attributes.Attributes = append(serviceMapCreate.In.Attributes.Attributes, attributes...)

	req := soapRequest
	req.Body.Method = serviceMapCreate
	data, err := xml.Marshal(req)
	if err != nil {
		return 0, err
	}

	var serviceMapCreateResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					Saoid  uint64 `xml:"saoid"`
				} `xml:"out"`
			} `xml:"serviceMapCreateResponse"`
		}
	}
	err = soapCall(data, &serviceMapCreateResponse)
	if err != nil {
		return 0, err
	}

	if serviceMapCreateResponse.Body.Response.Out.Err != 0 {
		return 0, errors.New(serviceMapCreateResponse.Body.Response.Out.ErrTxt)
	}

	return serviceMapCreateResponse.Body.Response.Out.Saoid, nil
}

//ServiceMapDelete deletes a service
func ServiceMapDelete(saoid uint64) error {
	var serviceMapDelete struct {
		XMLName xml.Name `xml:"becs:serviceMapDelete"`
		In      struct {
			Type  string `xml:"xsi:type,attr"`
			Saoid uint64 `xml:"saoid"`
		} `xml:"in"`
	}
	serviceMapDelete.In.Type = "becs:serviceMapDeleteIn"
	serviceMapDelete.In.Saoid = saoid

	req := soapRequest
	req.Body.Method = serviceMapDelete
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var serviceMapDeleteResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"serviceMapDeleteResponse"`
		}
	}
	err = soapCall(data, &serviceMapDeleteResponse)
	if err != nil {
		return err
	}

	if serviceMapDeleteResponse.Body.Response.Out.Err != 0 {
		return errors.New(serviceMapDeleteResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//ServiceMapDisable pauses a service
func ServiceMapDisable(saoid uint64) error {
	var serviceMapDisable struct {
		XMLName xml.Name `xml:"becs:serviceMapDisable"`
		In      struct {
			Type  string `xml:"xsi:type,attr"`
			Saoid uint64 `xml:"saoid"`
		} `xml:"in"`
	}
	serviceMapDisable.In.Type = "becs:serviceMapDisableIn"
	serviceMapDisable.In.Saoid = saoid

	req := soapRequest
	req.Body.Method = serviceMapDisable
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var serviceMapDisableResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"serviceMapDisableResponse"`
		}
	}
	err = soapCall(data, &serviceMapDisableResponse)
	if err != nil {
		return err
	}

	if serviceMapDisableResponse.Body.Response.Out.Err != 0 {
		return errors.New(serviceMapDisableResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//ServiceMapEnable activates a service
func ServiceMapEnable(saoid uint64) error {
	var serviceMapEnable struct {
		XMLName xml.Name `xml:"becs:serviceMapEnable"`
		In      struct {
			Type  string `xml:"xsi:type,attr"`
			Saoid uint64 `xml:"saoid"`
		} `xml:"in"`
	}
	serviceMapEnable.In.Type = "becs:serviceMapEnableIn"
	serviceMapEnable.In.Saoid = saoid

	req := soapRequest
	req.Body.Method = serviceMapEnable
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var serviceMapEnableResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"serviceMapEnableResponse"`
		}
	}
	err = soapCall(data, &serviceMapEnableResponse)
	if err != nil {
		return err
	}

	if serviceMapEnableResponse.Body.Response.Out.Err != 0 {
		return errors.New(serviceMapEnableResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//ServiceMapModify modifies a service
func ServiceMapModify(saoid uint64, attributes ...ServiceMapAttribute) (uint64, error) {
	var serviceMapModify struct {
		XMLName xml.Name `xml:"becs:serviceMapModify"`
		In      struct {
			Type       string `xml:"xsi:type,attr"`
			Saoid      uint64 `xml:"saoid"`
			Attributes struct {
				Attributes []ServiceMapAttribute `xml:"attributes"`
			} `xml:"attributes"`
		} `xml:"in"`
	}
	serviceMapModify.In.Type = "becs:serviceMapModifyIn"
	serviceMapModify.In.Saoid = saoid

	serviceMapModify.In.Attributes.Attributes = append(serviceMapModify.In.Attributes.Attributes, attributes...)

	req := soapRequest
	req.Body.Method = serviceMapModify
	data, err := xml.Marshal(req)
	if err != nil {
		return 0, err
	}

	var serviceMapModifyResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					Saoid  uint64 `xml:"saoid"`
				} `xml:"out"`
			} `xml:"serviceMapModifyResponse"`
		}
	}
	err = soapCall(data, &serviceMapModifyResponse)
	if err != nil {
		return 0, err
	}

	if serviceMapModifyResponse.Body.Response.Out.Err != 0 {
		return 0, errors.New(serviceMapModifyResponse.Body.Response.Out.ErrTxt)
	}

	return serviceMapModifyResponse.Body.Response.Out.Saoid, nil
}

//ServiceMapInfoGet collects info from a mapped service
func ServiceMapInfoGet(saoid uint64, categories []string) ([]StatusInfo, error) {
	var serviceMapInfoGet struct {
		XMLName xml.Name `xml:"becs:serviceMapInfoGet"`
		In      struct {
			Type       string `xml:"xsi:type,attr"`
			Saoid      uint64 `xml:"saoid"`
			Categories struct {
				Categories []string `xml:"categories"`
			} `xml:"categories,omitempty"`
		} `xml:"in"`
	}
	serviceMapInfoGet.In.Type = "becs:serviceMapInfoGetIn"
	serviceMapInfoGet.In.Saoid = saoid
	serviceMapInfoGet.In.Categories.Categories = categories

	req := soapRequest
	req.Body.Method = serviceMapInfoGet
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, err
	}

	var serviceMapInfoGetResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					Info   struct {
						Items []StatusInfo `xml:"item"`
					} `xml:"info,omitempty"`
				} `xml:"out"`
			} `xml:"serviceMapInfoGetResponse"`
		}
	}
	err = soapCall(data, &serviceMapInfoGetResponse)
	if err != nil {
		return nil, err
	}

	if serviceMapInfoGetResponse.Body.Response.Out.Err != 0 {
		return nil, errors.New(serviceMapInfoGetResponse.Body.Response.Out.ErrTxt)
	}

	return serviceMapInfoGetResponse.Body.Response.Out.Info.Items, nil
}

//JobCreate creates a new job for object changes
func JobCreate(timeout uint32) (uint64, error) {
	var jobCreate struct {
		XMLName xml.Name `xml:"becs:jobCreate"`
		In      struct {
			Type    string `xml:"xsi:type,attr"`
			Timeout uint32 `xml:"timeout"`
		} `xml:"in"`
	}
	jobCreate.In.Type = "becs:jobCreateIn"
	jobCreate.In.Timeout = timeout

	req := soapRequest
	req.Body.Method = jobCreate
	data, err := xml.Marshal(req)
	if err != nil {
		return 0, err
	}

	var jobCreateResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					JobID  uint64 `xml:"jobid"`
				} `xml:"out"`
			} `xml:"jobCreateResponse"`
		}
	}
	err = soapCall(data, &jobCreateResponse)
	if err != nil {
		return 0, err
	}

	if jobCreateResponse.Body.Response.Out.Err != 0 {
		return 0, errors.New(jobCreateResponse.Body.Response.Out.ErrTxt)
	}

	return jobCreateResponse.Body.Response.Out.JobID, nil
}

//JobVerify verifies a job
func JobVerify(jobid uint64, noCellVerify uint32) error {
	var jobVerify struct {
		XMLName xml.Name `xml:"becs:jobVerify"`
		In      struct {
			Type         string `xml:"xsi:type,attr"`
			JobID        uint64 `xml:"jobid"`
			NoCellVerify uint32 `xml:"nocellverify,omitempty"`
		} `xml:"in"`
	}
	jobVerify.In.Type = "becs:jobVerifyIn"
	jobVerify.In.JobID = jobid
	if noCellVerify != 0 {
		jobVerify.In.NoCellVerify = noCellVerify
	}

	req := soapRequest
	req.Body.Method = jobVerify
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var jobVerifyResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"jobVerifyResponse"`
		}
	}
	err = soapCall(data, &jobVerifyResponse)
	if err != nil {
		return err
	}

	if jobVerifyResponse.Body.Response.Out.Err != 0 {
		return errors.New(jobVerifyResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//JobCommit commits a job
func JobCommit(jobid uint64, description string) error {
	var jobCommit struct {
		XMLName xml.Name `xml:"becs:jobCommit"`
		In      struct {
			Type        string `xml:"xsi:type,attr"`
			JobID       uint64 `xml:"jobid"`
			Description string `xml:"description,omitempty"`
		} `xml:"in"`
	}

	jobCommit.In.Type = "becs:jobCommitIn"
	jobCommit.In.JobID = jobid
	if description != "" {
		jobCommit.In.Description = description
	}

	req := soapRequest
	req.Body.Method = jobCommit
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var jobCommitResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"jobCommitResponse"`
		}
	}
	err = soapCall(data, &jobCommitResponse)
	if err != nil {
		return err
	}

	if jobCommitResponse.Body.Response.Out.Err != 0 {
		return errors.New(jobCommitResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//JobRollback rollbacks a job
func JobRollback(jobid uint64) error {
	var jobRollback struct {
		XMLName xml.Name `xml:"becs:jobRollback"`
		In      struct {
			Type  string `xml:"xsi:type,attr"`
			JobID uint64 `xml:"jobid"`
		} `xml:"in"`
	}
	jobRollback.In.Type = "becs:jobRollbackIn"
	jobRollback.In.JobID = jobid

	req := soapRequest
	req.Body.Method = jobRollback
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var jobRollbackResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"jobRollbackResponse"`
		}
	}
	err = soapCall(data, &jobRollbackResponse)
	if err != nil {
		return err
	}

	if jobRollbackResponse.Body.Response.Out.Err != 0 {
		return errors.New(jobRollbackResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

//JobDestroy rollbacks and destroys a job
func JobDestroy(jobid uint64) error {
	var jobDestroy struct {
		XMLName xml.Name `xml:"becs:jobDestroy"`
		In      struct {
			Type  string `xml:"xsi:type,attr"`
			JobID uint64 `xml:"jobid"`
		} `xml:"in"`
	}
	jobDestroy.In.Type = "becs:jobDestroyIn"
	jobDestroy.In.JobID = jobid

	req := soapRequest
	req.Body.Method = jobDestroy
	data, err := xml.Marshal(req)
	if err != nil {
		return err
	}

	var jobDestroyResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
				} `xml:"out"`
			} `xml:"jobDestroyResponse"`
		}
	}
	err = soapCall(data, &jobDestroyResponse)
	if err != nil {
		return err
	}

	if jobDestroyResponse.Body.Response.Out.Err != 0 {
		return errors.New(jobDestroyResponse.Body.Response.Out.ErrTxt)
	}

	return nil
}

/*JobStatusGet gets current status for a job.

Returned status codes

0 Job is ready to accept object changes.

1 Unused

2 Job is being verified.

3 Job failed during verification.

4 Job verified correctly.

5 Job failed during commit.

6 Job is committed.
*/
func JobStatusGet(jobid uint64) (uint32, error) {
	var jobStatusGet struct {
		XMLName xml.Name `xml:"becs:jobStatusGet"`
		In      struct {
			Type  string `xml:"xsi:type,attr"`
			JobID uint64 `xml:"jobid"`
		} `xml:"in"`
	}
	jobStatusGet.In.Type = "becs:jobStatusGetIn"
	jobStatusGet.In.JobID = jobid

	req := soapRequest
	req.Body.Method = jobStatusGet
	data, err := xml.Marshal(req)
	if err != nil {
		return 0, err
	}

	var jobStatusGetResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					Status uint32 `xml:"status"`
				} `xml:"out"`
			} `xml:"jobStatusGetResponse"`
		}
	}
	err = soapCall(data, &jobStatusGetResponse)
	if err != nil {
		return 0, err
	}

	if jobStatusGetResponse.Body.Response.Out.Err != 0 {
		return 0, errors.New(jobStatusGetResponse.Body.Response.Out.ErrTxt)
	}

	return jobStatusGetResponse.Body.Response.Out.Status, nil
}

func MetricGet(name string) ([]Metric, error) {
	var metricGet struct {
		XMLName xml.Name `xml:"becs:metricGet"`
		In      struct {
			Type   string        `xml:"xsi:type,attr"`
			Name   string        `xml:"name"`
			Labels []MetricLabel `xml:"labels"`
		} `xml:"in"`
	}
	metricGet.In.Type = "becs:metricGetIn"
	metricGet.In.Name = name

	req := soapRequest
	req.Body.Method = metricGet
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, err
	}

	var metricGetResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err     uint   `xml:"err"`
					ErrTxt  string `xml:"errtxt,omitempty"`
					Metrics struct {
						Items []Metric `xml:"item"`
					} `xml:"metrics,omitempty"`
				} `xml:"out"`
			} `xml:"metricGetResponse"`
		}
	}
	err = soapCall(data, &metricGetResponse)
	if err != nil {
		return nil, err
	}

	if metricGetResponse.Body.Response.Out.Err != 0 {
		return nil, errors.New(metricGetResponse.Body.Response.Out.ErrTxt)
	}

	return metricGetResponse.Body.Response.Out.Metrics.Items, nil
}

func ApplicationList() ([]string, error) {
	var applicationList struct {
		XMLName xml.Name `xml:"becs:applicationList"`
		In      struct {
			Type string `xml:"xsi:type,attr"`
		} `xml:"in"`
	}
	applicationList.In.Type = "becs:applicationListIn"

	req := soapRequest
	req.Body.Method = applicationList
	data, err := xml.Marshal(req)
	if err != nil {
		return nil, err
	}

	var applicationListResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err    uint   `xml:"err"`
					ErrTxt string `xml:"errtxt,omitempty"`
					Names  struct {
						Items []string `xml:"item"`
					} `xml:"names"`
				} `xml:"out"`
			} `xml:"applicationListResponse"`
		}
	}
	err = soapCall(data, &applicationListResponse)
	if err != nil {
		return nil, err
	}

	if applicationListResponse.Body.Response.Out.Err != 0 {
		return nil, errors.New(applicationListResponse.Body.Response.Out.ErrTxt)
	}

	return applicationListResponse.Body.Response.Out.Names.Items, nil
}

func ApplicationStatusGet(name string, includePools bool) (Application, error) {
	application := Application{}

	var applicationStatusGet struct {
		XMLName xml.Name `xml:"becs:applicationStatusGet"`
		In      struct {
			Type         string `xml:"xsi:type,attr"`
			Name         string `xml:"name"`
			IncludePools bool   `xml:"includepools,omitempty"`
		} `xml:"in"`
	}
	applicationStatusGet.In.Type = "becs:applicationStatusGetIn"
	applicationStatusGet.In.Name = name
	applicationStatusGet.In.IncludePools = includePools

	req := soapRequest
	req.Body.Method = applicationStatusGet
	data, err := xml.Marshal(req)
	if err != nil {
		return application, err
	}

	var applicationStatusGetResponse struct {
		Body struct {
			Response struct {
				Out struct {
					Err          uint   `xml:"err"`
					ErrTxt       string `xml:"errtxt,omitempty"`
					Displayname  string `xml:"displayname"`
					Hostname     string `xml:"hostname"`
					Version      string `xml:"version"`
					UpTime       uint   `xml:"uptime"`
					StartTime    uint   `xml:"starttime"`
					CPUUsage     uint   `xml:"cpuusage"`
					CPUAverage60 uint   `xml:"cpuaverage60"`
					MemoryPools  struct {
						Items []MemoryPool `xml:"item"`
					} `xml:"memorypools,omitempty"`
				} `xml:"out"`
			} `xml:"applicationStatusGetResponse"`
		}
	}
	err = soapCall(data, &applicationStatusGetResponse)
	if err != nil {
		return application, err
	}

	if applicationStatusGetResponse.Body.Response.Out.Err != 0 {
		return application, errors.New(applicationStatusGetResponse.Body.Response.Out.ErrTxt)
	}

	application.Displayname = applicationStatusGetResponse.Body.Response.Out.Displayname
	application.Hostname = applicationStatusGetResponse.Body.Response.Out.Hostname
	application.Version = applicationStatusGetResponse.Body.Response.Out.Version
	application.UpTime = applicationStatusGetResponse.Body.Response.Out.UpTime
	application.StartTime = applicationStatusGetResponse.Body.Response.Out.StartTime
	application.CPUUsage = applicationStatusGetResponse.Body.Response.Out.CPUUsage
	application.CPUAverage60 = applicationStatusGetResponse.Body.Response.Out.CPUAverage60
	application.MemoryPools = applicationStatusGetResponse.Body.Response.Out.MemoryPools.Items

	return application, nil
}
